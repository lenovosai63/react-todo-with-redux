import {Tabs} from 'react-bootstrap'
import styled  from 'styled-components';

const StyledTabs=styled(Tabs)`
border-bottom:0;
color:#000;
a{
    color:#000
}
.nav-link{
    border:0
}
.nav-link.active {
    background-color: transparent;
    color: #000;
    border-bottom: 4px solid purple;
    opacity: 1;
    font-weight: 600;
    border-radius: 4px;
    &:hover{
    box-shadow: 0 0 24px 8px rgba(0, 0, 0, 0.2);
  }
  }

`
const TabsHeader = ({children,...rest}) => {
    return (
   <StyledTabs {...rest}>
     {children}
   </StyledTabs>
    )
}

export default TabsHeader
