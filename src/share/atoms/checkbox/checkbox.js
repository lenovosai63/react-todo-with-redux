import styled from "styled-components"
const CheckboxWrapper = styled.input.attrs(({ type}) => {
  return ({type:type||"checked"})
})`
   width:30px;
   height:20px;
   background:green;
   &:checked{
       color:red;
       background:green;
   }
  `

const Checkbox = ({...rest  }) => {
    return (
    <CheckboxWrapper {...rest}/>
    )
}

export default Checkbox
