import React from 'react'
import styled from 'styled-components'
const ButtonWrapper =styled.button`
width:120px;
border:0;
height:fit-content;
padding: 10px;
border-radius:8px;
color:#fff;
background-color: purple;
display: flex;
justify-content: center;
align-items: center;
`
const Button = ({addTodoHandler,disabled,buttonText}) => {
    return (
      <ButtonWrapper onClick={addTodoHandler} disabled={disabled}>{buttonText}</ButtonWrapper>
    )
}

export default Button
