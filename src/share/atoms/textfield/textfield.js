import React from "react"
import styled from 'styled-components'
const Inputwrapper= styled.input`
 width: ${props => (props.feature ? '704px' : '348px')};
  height: 30px;
  border-radius: 6px;
  padding-left: 6px;
  border: none;
  background-color: #efefef;
  font-family: Quicksand;
  font-size: 16px;
  font-weight: bold;
  padding: 24px;
  margin-right:20px;
  /* :placeholder-shown {
    padding: 24px;
  } */

  &:focus{
    outline:none;
  }

`
const TextField = ({handleTodo,todoText,...rest}) => {
    return (
   <Inputwrapper onChange={handleTodo} value={todoText} {...rest}/>
    )
}

export default TextField
