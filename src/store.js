import { TodoReducer } from 'container/homepage/reducer';
import { createStore} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
const store = createStore(
  TodoReducer,
  composeWithDevTools()
  
);
export default store