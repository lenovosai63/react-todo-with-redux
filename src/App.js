import TodoPage from "./container/homepage";
import {Provider}  from 'react-redux'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <Provider store={store}>
    <TodoPage/>
    </Provider>
  
  );
}

export default App;
