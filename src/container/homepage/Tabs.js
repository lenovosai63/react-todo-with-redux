import { useEffect, useState } from 'react';
import {  Tab ,Container} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux'
import { activeTodos, allTodos, completedTodos } from './actions';
import CompletedTodos from './CompletedTodos';
import ActiveTodos from './ActiveTodos';
import Todo from './Todo'
import TabsHeader from 'share/molecules/tabsheader';
const TabsPage = () => {
    const [tabSelect, setTabSelect] = useState("all")
    const state=useSelector(state=>state)
    const[initialDisable,setInitialDisable]=useState(true);
    useEffect(()=>{
        if(state.todos.length>0){
            setInitialDisable(false)
        }
    },[state])
    const dispatch = useDispatch();
    const handleTabs = (event) => {
        setTabSelect(event)
        if(event==="all"){
            dispatch(allTodos())
        }
        else if(event==="active"){
            dispatch(activeTodos())
        }
        else{
            dispatch(completedTodos())
        }
    }
    return (
        <>
        <TabsHeader id="controlled-tab-example" activeKey={tabSelect} onSelect={handleTabs}>
        <Tab eventKey="all" title="All"/>         
            <Tab eventKey="active" title="Active"/>
            <Tab eventKey="completed" title="Completed" disabled={initialDisable }/> 
        </TabsHeader>
        <Container>
            {tabSelect==="all"&&<Todo/>}
            {tabSelect==="active"&&<ActiveTodos/>}
            {tabSelect==="completed"&&<CompletedTodos/>}
        </Container>
       </>

    )
}

export default TabsPage
