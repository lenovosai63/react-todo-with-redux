import {MdEdit,MdDelete} from 'react-icons/md'
import { IconContext} from 'react-icons'
import Checkbox from 'share/atoms/checkbox';
import styled from 'styled-components';

const TodoWrapper=styled.div`
display: flex;
flex-direction:column;
`
const TodoText=styled.p`
margin-left:12px;
font-weight:bold;
font-size:15px;
line-height:1.5;
`
const Wrapper =styled.div`
margin-top:30px;
display: flex;
justify-content: space-between;
align-items:center;
max-width:500px;
width:80%;
@media screen and (max-width:400px)
{
  width:100%;
}
`
const IconsWrapper = styled.div`
display: flex;
height:40px;
`
const CommonTodoList = ({todos,handleCheckedTodos,handleDeleteTodo,handleEditTodo}) => {
    return (
        <TodoWrapper>
       {todos.map(todo => (
          <Wrapper key={todo.id}>
            <div style={{display:"flex"}}>
          <Checkbox type="checkbox" onChange={(event) => handleCheckedTodos(todo.id,event.target.checked)} checked={todo.active} />
          <TodoText>{todo.todoText}</TodoText>
          </div>
          <IconsWrapper>
          <IconContext.Provider
              value={{
                style: {
                  verticalAlign: 'middle',
                  width: '25px',
                  height: '25px',
                  color:"purple",
                  cursor:"pointer"
                },
              }}
            >
              <button onClick={()=>{
             handleEditTodo(todo.id)
             }} style={{border:0,background:"transparent",marginBottom:"30px"}}
             disabled={todo.disableEdit}
             >
              <MdEdit/>
              </button>
           
            </IconContext.Provider>
            <IconContext.Provider
              value={{
                style: {
                  verticalAlign: 'middle',
                  width: '25px',
                  height: '25px',
                  marginLeft: "30px",
                  color:"purple",
                  cursor:"pointer",
                },
              }}
            >
            <MdDelete onClick={()=>handleDeleteTodo(todo.id)}/>
            </IconContext.Provider>
          </IconsWrapper>
        
         </Wrapper>
      
      ))}
    </TodoWrapper>

    )
}

export default CommonTodoList
