import React, { useEffect, useState } from 'react'
import Button from 'share/atoms/button'
import TextField from 'share/atoms/textfield'
import TabsPage from './Tabs'
import {useDispatch, useSelector} from 'react-redux'
import { addTodo } from './actions'
import {ContainerWrapper,Wrapper,Heading} from './styles'


const TodoPage = () => {
    const dispatch =useDispatch();
    const editTodo= useSelector(state=>state.editTodo)
    const [todoText,setTodoText]=useState("")
    useEffect(()=>{
        if(editTodo.hasOwnProperty("id")){
            setTodoText(editTodo.todoText)
        }
        else{
            setTodoText("")
        }
    },[editTodo])
    const handleTodo=(e)=>setTodoText(e.target.value)
    const addTodoHandler=()=>{
        const id = editTodo.hasOwnProperty("id")?editTodo.id:new Date().getTime();
        const active =editTodo.hasOwnProperty("id")?editTodo.active:false
         dispatch(addTodo({id,todoText:todoText,active:active,disableEdit:false}))
         setTodoText("")
    }
    return (
        <ContainerWrapper>
       <Heading>Todos</Heading> 
       <Wrapper>
       <TextField handleTodo={handleTodo} todoText={todoText} placeholder="what need to be done?"/>
       <Button addTodoHandler={addTodoHandler} disabled={todoText.length<2} buttonText={editTodo.hasOwnProperty("id")?"edit":"Add"}/>
       </Wrapper>
       <TabsPage/>
       </ContainerWrapper>
    )
}

export default TodoPage
