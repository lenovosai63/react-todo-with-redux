
import { useSelector, useDispatch } from 'react-redux'
import { checkedTodos, deleteTodo, editTodo } from './actions';
import CommonTodoList from './CommonTodoList';
const Todo = (props) => {
  let totalState = useSelector(state => state)
  const dispatch = useDispatch();
  const handleCheckedTodos=(id,isChecked)=>{
    dispatch(checkedTodos(id,isChecked))
  }

  const handleDeleteTodo=(id)=>{
     dispatch(deleteTodo(id))
  }
  const handleEditTodo=(id )=>{
     dispatch(editTodo(id))
  }
  return (
    <CommonTodoList todos={totalState.todos}  handleCheckedTodos={handleCheckedTodos}
    handleDeleteTodo={handleDeleteTodo}
    handleEditTodo={handleEditTodo}
    />
  )
}

export default Todo
