import styled from "styled-components";
export const ContainerWrapper=styled.div`
 max-width:700px;
 margin:40px auto;
 background-color:#e6e6fa;
 padding:20px;
 width:90%;

`
export const Wrapper = styled.div`
display: flex;
margin-bottom:30px;
`
export const Heading = styled.h1`
text-align:center;
margin-bottom:30px;
`