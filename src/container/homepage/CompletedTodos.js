
import { useSelector, useDispatch } from 'react-redux'
import { completedTodos,checkedTodos, deleteTodo, editTodo } from './actions';
import CommonTodoList from './CommonTodoList';
const CompletedTodos = (props) => {
    let totalState = useSelector(state => state)
    const dispatch = useDispatch();
    const handleCheckedTodos=(id,isChecked)=>{
        dispatch(checkedTodos(id,isChecked))
        dispatch(completedTodos())
      }
      const handleDeleteTodo=(id)=>{
        dispatch(deleteTodo(id))
        dispatch(completedTodos())
     }
     const handleEditTodo=(id)=>{
       dispatch(editTodo(id))
     }
    return (
    <CommonTodoList todos={totalState.completedTodos} handleCheckedTodos={handleCheckedTodos} 
    handleDeleteTodo={handleDeleteTodo}
    handleEditTodo={handleEditTodo}
    />
    )
}

export default CompletedTodos
