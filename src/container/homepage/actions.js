import { ACTIVETODOS, ADDTODO, ALLTODOS, CHECKEDTODS, COMPLETEDTODS, DELETETODO, EDITTODO } from "./constants"

export const addTodo=(todos)=>{
    return{
        type:ADDTODO,
        payload:todos
    }
}

export const allTodos=()=>{
    return{
        type:ALLTODOS,
    }
}

export const activeTodos=()=>{
    return {
        type:ACTIVETODOS
    }
}

export const completedTodos=()=>{
    return {
        type:COMPLETEDTODS
    }
}

export const checkedTodos=(id,isChecked)=>{
    return{
        type:CHECKEDTODS,
        payload:{id:id,isChecked:isChecked}
    }
}

export const deleteTodo=(id)=>{
    console.log(id,"id in delete")
    return{
        type:DELETETODO,
        payload:{id}
    }
}
export const editTodo=(id)=>{
    return{
        type:EDITTODO,
        payload:{id}
    }
}