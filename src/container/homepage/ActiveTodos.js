import { useSelector, useDispatch } from 'react-redux'
import { activeTodos,checkedTodos, deleteTodo, editTodo } from './actions';
import CommonTodoList from './CommonTodoList';
const ActiveTodos = (props) => {
    let totalState = useSelector(state => state)
    const dispatch = useDispatch();
    const handleCheckedTodos=(id,isChecked)=>{
        dispatch(checkedTodos(id,isChecked))
        dispatch(activeTodos())
      }
      const handleDeleteTodo=(id)=>{
        dispatch(deleteTodo(id))
        dispatch(activeTodos())
     }
     const handleEditTodo=(id )=>{
        dispatch(editTodo(id))
     }
    return (
        <CommonTodoList todos={totalState.activeTodos} handleCheckedTodos={handleCheckedTodos}
        handleDeleteTodo={handleDeleteTodo}
        handleEditTodo={handleEditTodo}
        />
    )
}

export default ActiveTodos
