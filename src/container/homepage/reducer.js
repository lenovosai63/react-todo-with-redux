import { ACTIVETODOS, ADDTODO, ALLTODOS, CHECKEDTODS, COMPLETEDTODS, DELETETODO, EDITTODO } from "./constants";
const initialstate = {
    todos: [],
    activeTodos: [],
    completedTodos: [],
    editTodo: {}
}
export const TodoReducer = (state = initialstate, action) => {
    switch (action.type) {
        case ADDTODO:
                return {
                    ...state,
                    todos: [...state.todos.map(todo=>({...todo,disableEdit:false})),action.payload],
                    activeTodos:[...state.activeTodos.map(todo=>({...todo,disableEdit:false})),action.payload],
                    completedTodos:[...state.completedTodos.map(todo=>({...todo,disableEdit:false})),action.payload],
                    editTodo:{}
                }
            
        case ALLTODOS:
            return state

        case CHECKEDTODS:
            return {
                ...state,
                todos: state.todos.map(todo => todo.id === action.payload.id ? { ...todo, active: action.payload.isChecked } : todo)
            }
        case ACTIVETODOS:
            return {
                ...state,
                activeTodos: state.todos.filter(todo => !todo.active)
            }

        case COMPLETEDTODS:
            return {
                ...state,
                completedTodos: state.todos.filter(todo => todo.active)
            }

        case DELETETODO:
            return {
                ...state,
                todos: state.todos.filter(todo => todo.id !== action.payload.id)
            }

        case EDITTODO:
            return {
                ...state,
                editTodo: state.todos.find(todo => todo.id === action.payload.id),
                todos:state.todos.filter(todo=>todo.id!==action.payload.id).map(todo=>todo.id===action.payload.id?{...todo,disableEdit:false}:{...todo,disableEdit:true}),
                activeTodos:state.activeTodos.filter(todo=>todo.id!==action.payload.id).map(todo=>todo.id===action.payload.id?{...todo,disableEdit:false}:{...todo,disableEdit:true}),
                completedTodos:state.completedTodos.filter(todo=>todo.id!==action.payload.id).map(todo=>todo.id===action.payload.id?{...todo,disableEdit:false}:{...todo,disableEdit:true}),
            }
        default:
            return state;
    }
}